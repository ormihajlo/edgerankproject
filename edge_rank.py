from datetime import datetime, timedelta
import networkx as nx

scores = {
    "reaction": 15,
    "comment": 45,
    "share": 150
}

def affinity_decay(time):
    if time > datetime.now() - timedelta(days=7):
        return 1
    elif time > datetime.now() - timedelta(days=30):
        return 0.1
    return 0.01

def status_weight(post):
    reactions = post["num_reactions"] * 15
    comments = post["num_comments"] * 45
    shares = post["num_shares"] * 150

    return reactions + comments + shares

def time_decay(time):
    
    if time > datetime.now() - timedelta(days=1):
        decay = 1
        return decay
    elif time > datetime.now() - timedelta(days=7):
        decay = 50000
        return decay
    
    else:
        decay = 1000000
        return decay


def reaction_score(name_1, name_2, test_reactions, test_statuses):

    reaction_score = 0
    for reaction in test_reactions[name_1]:
        status_id = reaction["status_id"]   
        if name_2 == test_statuses[status_id]["author"]:
            date = reaction["reacted"]
            reaction_score += scores["reaction"] * affinity_decay(date)
    
    return reaction_score

def comment_score(name_1, name_2, test_comments, test_statuses):

    comment_score = 0
    if name_1 in test_comments:
        for comment in test_comments[name_1]:
            status_id = comment["status_id"]
            if name_2 == test_statuses[status_id]["author"]:
                date = comment["comment_published"]
                comment_score += scores["comment"] * affinity_decay(date)
    
    return comment_score

def share_score(name_1, name_2, test_shares, test_statuses):

    share_score = 0
    if name_1 in test_shares:
        for share in test_shares[name_1]:
            status_id = share["status_id"]
            if name_2 == test_statuses[status_id]["author"]:
                date = share["status_shared"]
                share_score += scores["share"] * affinity_decay(date)
    
    return share_score

def affinity(name_1, name_2, test_shares, test_reactions, test_comments, test_statuses):

    score = share_score(name_1, name_2, test_shares, test_statuses) + comment_score(name_1, name_2, test_comments, test_statuses) + reaction_score(name_1, name_2, test_reactions, test_statuses)
    return score

# def add_friends_of_friends_graph(current, g:nx.DiGraph(), test_users):

#     friends = test_users[current]
#     for friend in friends:
#         edges = g.out_edges(friend)
#         for edge in edges:
#             score = g[edge[0]][edge[1]]["weight"] / 100
#             if g.has_edge(current, edge[1]):
#                 g[current][edge[1]]["weight"] += score
#             else:
#                 g.add_edge(current, edge[1], weight=score)

def complete_graph(graph:nx.DiGraph(), users, shares, statuses, comments, reactions):

    for name_1 in users:
        for name_2 in users:

            if name_1 == name_2:
                continue

            score = affinity(name_1, name_2, shares, reactions, comments, statuses)

            if score > 0:
                if graph.has_edge(name_1, name_2):
                    graph[name_1][name_2]["weight"] += score
                else:
                    graph.add_edge(name_1, name_2, weight=score)
    
    return graph


def create_graph(test_users, test_shares, test_statuses, test_comments, test_reactions):

    g = nx.DiGraph()

    for name_1 in test_users:               #name 1 je korisnik, name 2 je publisher
        for name_2 in test_users:

            if name_1 == name_2:
                continue
            
            g.add_node(name_1)
            g.add_node(name_2)
            
            

            score = affinity(name_1, name_2, test_shares, test_reactions, test_comments, test_statuses)

            if name_2 in test_users[name_1]:
                score += 500000

            if score > 0:
                g.add_edge(name_1, name_2, weight=score)
    
    return g

def find_strongest_bonds(g:nx.DiGraph(), user):
    users = []

    edges = g.out_edges(user)
    sorted_edges = sorted(edges, key=lambda edge: g[edge[0]][edge[1]]['weight'], reverse=True)

    for edge in sorted_edges:
        users.append(edge[1])
    
    return users


def find_best_statuses(users, test_statuses, current, g:nx.DiGraph(), **kwargs):
    l = []                                              #lists in list, elemets of inner lists are status, score
    dict = test_statuses
    for k,v in kwargs.items():                          #prilikom pretrage koji status se koliko puta pojavljivao
        dict = v

    affinity = 0

    for user in users:
        if g.has_edge(current, user):
            affinity += g.get_edge_data(current, user)["weight"]
        for id in test_statuses:
            if id in dict:
                if test_statuses[id]["author"] == user:
                    weight = status_weight(test_statuses[id]) + affinity * 100000
                    if id in dict and isinstance(dict[id], int):
                        weight += 100000*dict[id]
                    decay = time_decay(test_statuses[id]["status_published"])
                    score = weight/decay
                    l.append([test_statuses[id], score])
    
    sorted_list = sorted(l, key=lambda el: el[1], reverse=True)
    sorted_cut_list = sorted_list[:10]
    statuses = list(map(lambda x:x[0], sorted_cut_list))                #want to put only statuses in list, dont care about the score here
    return statuses

def find_best_statuses_with_trie(users, test_statuses, current, g:nx.DiGraph(), trie_dict):
    l = []                                              #lists in list, elemets of inner lists are status, score

    affinity = 0

    for user in users:
        if g.has_edge(current, user):
            affinity += g.get_edge_data(current, user)["weight"]
        for id in trie_dict:
            if test_statuses[id]["author"] == user:
                weight = status_weight(test_statuses[id]) + affinity * 100000
                weight += 100000000*trie_dict[id]
                decay = time_decay(test_statuses[id]["status_published"])
                score = weight/decay
                l.append([test_statuses[id], score])
    
    sorted_list = sorted(l, key=lambda el: el[1], reverse=True)
    sorted_cut_list = sorted_list[:10]
    statuses = list(map(lambda x:x[0], sorted_cut_list))                #want to put only statuses in list, dont care about the score here
    return statuses


def print_statuses(statuses):

    print()
    print("***Top posts for today***")

    for status in statuses:
        content = status["status_message"]
        num_reactions = status["num_reactions"]
        num_comments = status["num_comments"]
        num_shares = status["num_shares"]
        author = status["author"]

        modified_content = ""

        for c in content:
            if c in [".", "?", "!"] and modified_content[-1] == "\n":
                modified_content = modified_content[:-1]
            modified_content += c
            if c in [".", "!", "?"]:
                modified_content += "\n"

        print()
        print(30* "-")
        print()
        print("Author: " + author)
        print()
        print(modified_content)
        print()
        print("Likes: " + str(num_reactions))
        print("Comments: " + str(num_comments))
        print("Shares: " + str(num_shares))
    print()




