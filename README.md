This is a second and final project for my second semester subject Algorithms and data structures. Graph and Trie are located in the folder called objects. The folder called new_graph.pkl contains an improved graph(in terms of values of a reaction share comment...) compared to the one located in the file called graph.pkl. And finally the file called enhanced.pkl contains graph with data from original and test files.

The file called main.py is the file that you should run in order to start the application.

Also an important this to mention is that I havent made a difference between a like, dislike, angry reaction etc... Because Youtube, instagram, facebook all value these interactions the same. That is because if you are constantly disliking someones posts they will keep appearing in your feed for you to dislike them again. Same goes for likes and other reactions...

One more thing, currently I have inserted only words from original statuses in my trie so word searching works only for original statuses.
