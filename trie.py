class Node(object):

    def __init__(self, char):
        self._char = char
        self._children = {}
        self._is_finished = False
        self._ids = {}  #   id:broj pojavljivanja
    
class Trie(object):

    def __init__(self):
        self._root = Node("")
    
    def insert_status(self, status, id):

        words = status.split(" ")
        for word in words:
            node = self._root
            for char in word:
                if char not in node._children:
                    new_node = Node(char)
                    node._children[char] = new_node
                    node = new_node
                else:
                    node = node._children[char]
            node._is_finished = True
            if id in node._ids:
                node._ids[id] += 1
            node._ids[id] = 1


    def find_word(self, word):
        
        helper = ""
        node = self._root
        for char in word:
            if char not in node._children:
                return None
            helper += char
            node = node._children[char]
            if helper == word:
                return node._ids

    def find_prefix(self, prefix):

        helper = ""
        node = self._root

        for char in prefix:
            if char not in node._children:
                return None
            helper += char
            node = node._children[char]
            if helper == prefix:
                break
        l = self.dfs(node,prefix, [])

        return l


    def dfs(self, node, prefix, l):

        for char in node._children:
            if node._children != {}:
                prefix += char
                if node._children[char]._is_finished:
                    l.append(prefix)
                self.dfs(node._children[char],prefix, l)
                prefix = prefix[:-1]
        return l
    
#filtering statuses for inserting words into trie
def filter_status(status):

    new = ""
    for char in status:
        char = char.lower()
        if (ord(char) >= 97 and ord(char) <= 122) or ord(char) == 32:
            new += char
        else:
            new += " "

    return new.strip()
    
def trie_work(trie:Trie, inputt):

    inputt = inputt.lower().strip()
    
    if inputt.endswith("*"):
        inputt = inputt[:-1]
        l = trie.find_prefix(inputt)
        if l is not None:
            print_list(l)
            while True:
                try:
                    option = int(input("Choose a number in front of a word>>> "))
                    if option < 1 or option > len(l):
                        raise Exception
                    break
                except:
                    print(Exception("Unos nije validan!"))
            word = l[option-1]
            ids = trie.find_word(word)
            return ids
        else:
            print("There is no post that includes your input.")
    
    elif inputt.endswith("\""):                          
        inputt = inputt[1:-1]
        new = filter_status(inputt)
        list = new.split(" ")
        list_of_dicts = []
        for word in list:
            if word != "":
                dict = trie.find_word(word)
                if dict is not None:
                    list_of_dicts.append(dict)
        if list_of_dicts == []:
            return None
        dictionary = merge_dictionaries_same_keys(list_of_dicts)
        return dictionary
    
    elif " " in inputt:
        list_of_dicts = []
        list = inputt.split(" ")
        for word in list:
            dict = trie.find_word(word)
            list_of_dicts.append(dict)
        
        if list_of_dicts == []:
            return None
        dictionary = merge_dictionaries(list_of_dicts)
        return dictionary

    else:
        ids = trie.find_word(inputt)
        if ids is not None:
            return ids
        return None


def print_list(list):
    for i in range(len(list)):
        print(str(i+1) + ". " + list[i])


#for multiple word search
def merge_dictionaries(l):

    new_dict = {}
    for dict in l:
        if dict is not None:
            for k in dict:
                if k not in new_dict:
                    new_dict[k] = dict[k]
                else:
                    new_dict[k] += dict[k]
    return new_dict


#for phrase searching, every word needs to be in status
def merge_dictionaries_same_keys(l):

    new_dict = {}
    for dict in l:
        for k in dict:
            if k not in new_dict:
                new_dict[k] = dict[k]
            else:
                new_dict[k] += dict[k]
    deletion = []
    for k in new_dict:
        for dict in l:
            if k not in dict:
                deletion.append(k)
                break
    
    for k in deletion:
        del new_dict[k]
    
    return new_dict
    

