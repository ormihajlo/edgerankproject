from helper import load_objects, load_data, main_method

print("Forming Graph and Trie...")
graph, trie = load_objects()
statuses, users = load_data()
print("Completed!")
print()

main_method(users, statuses, graph, trie)