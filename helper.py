import pickle
import parse_files
from boyer_moore import boyer_moore
from trie import trie_work
from edge_rank import print_statuses, find_best_statuses, find_strongest_bonds, find_best_statuses_with_trie
from sys import exit

#function that makes a new dictionary containing only certain keys
def filter_dictionary(original_dict, keys_dict):
    filtered_dict = {key: original_dict[key] for key in keys_dict.keys() if key in original_dict}
    return filtered_dict

#for phrase searching
def find_phrase_in_statuses(statuses, phrase):

    dict = {}
    
    for id in statuses:
        if boyer_moore(statuses[id]["status_message"], phrase) != -1:
            dict[id] = statuses[id]
    
    return dict


def load_objects():

    with open("objects/enhanced.pkl", "rb") as file:
        g = pickle.load(file)
    with open("objects/trie.pkl", "rb") as file:
        t = pickle.load(file)
    
    return g, t

def load_data():
    statuses = parse_files.load_statuses("dataset/original_statuses.csv")
    users = parse_files.load_friends("dataset/friends.csv")
    return statuses, users

def login(name, users:dict):
    if name in users:
        return name
    return None


#main method that handles users input
def main_method(users, statuses, graph, trie):
    while True:
        name = input("Type your name>>> ")
        if name == "exit":
            exit(0)
        user = login(name, users)
        if user is None:
            print("Invalid name, type exit if you don't want to try logging in one more time")
        else:
            print("You logged in successfully")
            print()
            while True:
                print("Choose one option")
                print()
                print("1. Check Best Statuses")
                print("2. Search")
                print("3. Exit")
                print()
                choice = input("Type one number in front of the above listed options>>> ")
                if choice == "1":
                    list = find_strongest_bonds(graph, user)
                    list_statuses = find_best_statuses(list, statuses, user, graph)
                    print_statuses(list_statuses)
                elif choice == "2":
                    print()
                    search = input("Search your feed>>> ")
                    dictionary = trie_work(trie, search)
                    if dictionary is None:
                        print("Nothing matches your search criteria :(")
                        continue
                    if search.endswith("\""):
                        phrase = search[1:-1]
                        filtered = filter_dictionary(statuses, dictionary)
                        new_statuses = find_phrase_in_statuses(filtered, phrase)
                        list = find_best_statuses(users, new_statuses, user, graph)
                        print_statuses(list)
                    else:
                        list = find_best_statuses_with_trie(users, statuses, user, graph, dictionary)
                        print_statuses(list)
                elif choice == "3":
                    print("The application is closing...")
                    exit(0)
                else:
                    print("Invalid choice")