import parse_files
import edge_rank
import pickle
from itertools import islice
from trie import Trie


#users = parse_files.load_friends("dataset/friends.csv")
# # first_three = dict(islice(users.items(), 100))
# reactions = parse_files.load_reactions("dataset/original_reactions.csv")
# shares = parse_files.load_shares("dataset/original_shares.csv")
# comments = parse_files.load_comments("dataset/original_comments.csv")
# statuses = parse_files.load_statuses("dataset/original_statuses.csv")

# test_reactions = parse_files.load_reactions("dataset/test_reactions.csv")
# test_shares = parse_files.load_shares("dataset/test_shares.csv")
# test_comments = parse_files.load_comments("dataset/test_comments.csv")
# test_statuses = parse_files.load_statuses("dataset/test_statuses.csv")

#g = edge_rank.create_graph(users, shares, statuses, comments, reactions)
# t = Trie()
# for id in statuses:
#     filtered = filter_status(statuses[id]["status_message"])
#     t.insert_status(filtered,id)

# with open("objects/new_graph.pkl", "wb") as f:
#     pickle.dump(g, f)

# with open("objects/trie.pkl", "wb") as file:
#     pickle.dump(t, file)

# with open("objects/new_graph.pkl", "rb") as file:
#     g = pickle.load(file)

# enhanced = edge_rank.complete_graph(g, users, test_shares, test_statuses, test_comments, test_reactions)

# with open("objects/enhanced.pkl", "wb") as file:
#     pickle.dump(enhanced, file)

# with open("objects/enhanced.pkl", "rb") as file:
#     enhanced = pickle.load(file)

# with open("objects/trie.pkl", "rb") as file:
#     t = pickle.load(file)

# for edge in g.out_edges("Patsy Arseneau"):
#     print(g[edge[0]][edge[1]]['weight'])
#     print(edge)
# print("regular graph: ")
# print("Edges: " + str(g.number_of_edges()))
# print("Nodes: " + str(g.number_of_nodes()))
# print("Enhanced graph: ")
# print("Edges: " + str(enhanced.number_of_edges()))
# print("Nodes: " + str(enhanced.number_of_nodes()))

